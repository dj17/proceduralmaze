// Fill out your copyright notice in the Description page of Project Settings.

#include "Procedural.h"
#include "P_Maze.h"


// Sets default values
AP_Maze::AP_Maze(const FObjectInitializer& ObjectInitializer):Super(ObjectInitializer)
{
	_scene = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("Scene"));
	RootComponent = _scene;
	sizeX = 20;
	sizeY = 20;
	tileSize = 400;
	delay = 0.01f;
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AP_Maze::BeginPlay()
{
	Super::BeginPlay();
	
	for (int32 i = 0; i < sizeX; i++)
	{
		FMazeRow Row;
		for (int32 j = 0; j < sizeY; j++)
		{

			Row.Column.Add(NULL);

		}
		Grid.Add(Row);
	}
	
	DoFirstGenerationStep();
	GenerateGrid();
	
}

// Called every frame
void AP_Maze::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}
void AP_Maze::GenerateGrid()
{	
	//DoFirstGenerationStep();
	/*while (ActiveCells.Num() > 0)
	{
		DoNextGenerationStep();
	}*/
	if (ActiveCells.Num() > 0)
	{
		DoNextGenerationStep();
		GetWorldTimerManager().SetTimer(this, &AP_Maze::GenerateGrid, delay, true);
	}
	
}
void AP_Maze::Delay()
{
	GetWorldTimerManager().SetTimer(this, &AP_Maze::DoNextGenerationStep, 0.01f, false);
}

AP_MazeCell* AP_Maze::CreateCell(FIntVector coordinates)
{
	FVector Position = FVector(coordinates.Y*tileSize, coordinates.X*tileSize ,0);
	cells.Add(FIntVector(coordinates.Y*tileSize, coordinates.X*tileSize, 0));
	AP_MazeCell* Cell = GetWorld()->SpawnActor<AP_MazeCell>(AP_MazeCell::StaticClass(), Position, FRotator::ZeroRotator);
	Cell->Coordinates = coordinates;
	Grid[coordinates.X].Column[coordinates.Y] = Cell;
	return Cell;
}

FIntVector AP_Maze::RandomCoordinates()
{
	return FIntVector(FMath::RandRange(0,sizeX-1), FMath::RandRange(0,sizeY-1),0);
}

bool AP_Maze::ContainsCoordinates(FIntVector coordinates)
{
	return (coordinates.X >= 0 && coordinates.X < sizeX && coordinates.Y >= 0 && coordinates.Y < sizeY);
}

AP_MazeCell* AP_Maze::GetCell(FIntVector coordinates)
{
	return Grid[coordinates.X].Column[coordinates.Y];
}

void AP_Maze::DoNextGenerationStep()
{
	
		directions = new P_MazeDirection();
		int32 currentIndex = ActiveCells.Num() - 1;
		AP_MazeCell* CurrentCell = ActiveCells[currentIndex];

		if (CurrentCell->IsFullyInitialized())
		{
			ActiveCells.RemoveAt(currentIndex);
			return;
		}

		Direction randomDir = CurrentCell->RandomUninitializedDirection();
		//Direction randomDir = directions->RandomDirection();

		FIntVector coordinates = CurrentCell->Coordinates + directions->ToFIntVector(randomDir);
		if (ContainsCoordinates(coordinates))
		{
			AP_MazeCell* neighbour = GetCell(coordinates);
			if (neighbour == NULL)
			{
				neighbour = CreateCell(coordinates);
				CreatePassage(CurrentCell, neighbour, directions, randomDir);
				ActiveCells.Add(neighbour);
			}
			else
			{
				CreateWall(CurrentCell, neighbour, directions, randomDir);
			}
		}
		else
		{
			CreateWall(CurrentCell, NULL, directions, randomDir);
		}
}

void AP_Maze::DoFirstGenerationStep()
{
	ActiveCells.Add(CreateCell(RandomCoordinates()));
}

void AP_Maze::CreateWall(AP_MazeCell* cell, AP_MazeCell* otherCell, P_MazeDirection* dirObject, Direction dir)
{
	AP_MazeWall* wall = GetWorld()->SpawnActor<AP_MazeWall>(AP_MazeWall::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	if (dir == Direction::North || dir == Direction::South)
	{
		wall->GetStaticMeshComponent()->SetStaticMesh(wall->NorthSouthWall);
	}
	else
	{
		wall->GetStaticMeshComponent()->SetStaticMesh(wall->EastWestWall);
	}
	wall->Initialize(cell, otherCell, dirObject, dir);

	if (otherCell != NULL)
	{
		AP_MazeWall* anotherWall = GetWorld()->SpawnActor<AP_MazeWall>(AP_MazeWall::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
		if (dir == Direction::North || dir == Direction::South)
		{
			anotherWall->GetStaticMeshComponent()->SetStaticMesh(wall->NorthSouthWall);
		}
		else
		{
			anotherWall->GetStaticMeshComponent()->SetStaticMesh(wall->EastWestWall);
		}
		
		anotherWall->Initialize(otherCell, cell, dirObject , dirObject->GetOpposite(dir));
	}
}

void AP_Maze::CreatePassage(AP_MazeCell* cell, AP_MazeCell* otherCell, P_MazeDirection* dirObject, Direction dir)
{
	AP_MazePassage* passage = GetWorld()->SpawnActor<AP_MazePassage>(AP_MazePassage::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	passage->Initialize(cell, otherCell, dirObject , dir);
	
	AP_MazePassage* anotherPassage = GetWorld()->SpawnActor<AP_MazePassage>(AP_MazePassage::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	anotherPassage->Initialize(otherCell, cell, dirObject, dirObject->GetOpposite(dir));
}
