// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "P_MazeCell.h"
#include "P_MazeWall.h"
#include "P_MazePassage.h"
#include "P_MazeDirection.h"
#include "P_Maze.generated.h"

USTRUCT()
struct FMazeRow
{
	GENERATED_USTRUCT_BODY()
	TArray<AP_MazeCell*> Column;
};


UCLASS()
class PROCEDURAL_API AP_Maze : public AActor
{
	GENERATED_BODY()
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* _scene;
		
public:	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Size)
		float delay;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Size)
		int32 sizeX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Size)
		int32 sizeY;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Size)
		int32 tileSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Size)
		FIntVector positions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Size)
	TArray<FIntVector> cells;
	
	FTimerHandle timerHandle;

	TArray<FMazeRow> Grid;

	TArray<AP_MazeCell*> ActiveCells;

	P_MazeDirection* directions;

	// Sets default values for this actor's properties
	AP_Maze(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	FIntVector RandomCoordinates();
	bool ContainsCoordinates(FIntVector coordinates);
	void GenerateGrid();
	AP_MazeCell* CreateCell(FIntVector coordinates);
	AP_MazeCell* GetCell(FIntVector coordinates);
	void DoFirstGenerationStep();
	void DoNextGenerationStep();

	void CreateWall(AP_MazeCell* cell, AP_MazeCell* otherCell,P_MazeDirection* dirObject , Direction dir);
	void CreatePassage(AP_MazeCell* cell, AP_MazeCell* otherCell, P_MazeDirection* dirObject, Direction dir);

	void Delay();
};
