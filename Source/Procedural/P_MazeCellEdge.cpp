// Fill out your copyright notice in the Description page of Project Settings.

#include "Procedural.h"
#include "P_MazeCellEdge.h"


// Sets default values
AP_MazeCellEdge::AP_MazeCellEdge(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
	_scene = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("Scene"));
	RootComponent = _scene;
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NorthSouthWallMesh(TEXT("StaticMesh'/Game/StarterContent/Architecture/NorthSouthWall.NorthSouthWall'"));
	NorthSouthWall = NorthSouthWallMesh.Object;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> EastWestWallMesh(TEXT("StaticMesh'/Game/StarterContent/Architecture/EastWestWall.EastWestWall'"));
	EastWestWall = EastWestWallMesh.Object;

	EdgeDirection = Direction::North;

	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AP_MazeCellEdge::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AP_MazeCellEdge::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );

}

void AP_MazeCellEdge::Initialize(AP_MazeCell* cell, AP_MazeCell* otherCell, P_MazeDirection* edgeDirection , Direction dir)
{
	this->Cell = cell;
	this->OtherCell = otherCell;
	this->EdgeDirection = dir;
	this->Cell->SetEdge(dir, this);
	this->AttachRootComponentToActor(Cell, NAME_None, EAttachLocation::SnapToTarget);
	switch (dir)
	{
		case  Direction::North :
			this->SetActorRelativeLocation(FVector(200,0,200));
			
			break;
		case  Direction::East :
			this->SetActorRelativeLocation(FVector(0, 200, 200));
			
			break;
		case  Direction::South :
			this->SetActorRelativeLocation(FVector(-200, 0, 200));
			
			break;
		case  Direction::West :
			this->SetActorRelativeLocation(FVector(0, -200, 200));
			
			break;
	}
}

