// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "P_MazeCellEdge.h"
#include "P_MazeWall.generated.h"

UCLASS()
class PROCEDURAL_API AP_MazeWall : public AP_MazeCellEdge
{
	GENERATED_BODY()
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* _wall;
		
public:	
	
	// Sets default values for this actor's properties
	AP_MazeWall(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Return Static Mesh Component
	FORCEINLINE class UStaticMeshComponent* GetStaticMeshComponent()const { return _wall; }
	
};
