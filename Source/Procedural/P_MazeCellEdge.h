// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "P_MazeDirection.h"
#include "P_MazeCell.h"
#include "P_MazeCellEdge.generated.h"

UCLASS()
class PROCEDURAL_API AP_MazeCellEdge : public AActor
{
	GENERATED_BODY()
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* _scene;
		
public:	
	AP_MazeCell* Cell;
	AP_MazeCell* OtherCell;
	
	UPROPERTY(VisibleAnywhere, Category = Variables)
	TEnumAsByte<Direction> EdgeDirection;

	// Sets default values for this actor's properties
	AP_MazeCellEdge(const FObjectInitializer& ObjectInitializer);

	UStaticMesh* NorthSouthWall;
	UStaticMesh* EastWestWall;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
	
	void Initialize(AP_MazeCell* cell, AP_MazeCell* otherCell,P_MazeDirection* edgeDirection , Direction dir);
	
	
};
