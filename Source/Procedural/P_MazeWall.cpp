// Fill out your copyright notice in the Description page of Project Settings.

#include "Procedural.h"
#include "P_MazeWall.h"


// Sets default values
AP_MazeWall::AP_MazeWall(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{

	_wall = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Wall"));
	_wall->AttachTo(RootComponent);

	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AP_MazeWall::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AP_MazeWall::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );

}

