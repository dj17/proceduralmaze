// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "ProceduralGameMode.generated.h"

UCLASS(minimalapi)
class AProceduralGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AProceduralGameMode(const FObjectInitializer& ObjectInitializer);
};



