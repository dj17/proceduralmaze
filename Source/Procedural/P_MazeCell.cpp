// Fill out your copyright notice in the Description page of Project Settings.

#include "Procedural.h"
#include "P_MazeCell.h"


// Sets default values
AP_MazeCell::AP_MazeCell(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
	_scene = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("Scene"));
	RootComponent = _scene;

	_floor = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("floor"));
	_floor->AttachTo(_scene);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> FloorMesh(TEXT("StaticMesh'/Game/StarterContent/Architecture/Floor.Floor'"));
	_floor->StaticMesh = FloorMesh.Object;

	Edges.Add(NULL);
	Edges.Add(NULL);
	Edges.Add(NULL);
	Edges.Add(NULL);

	InitializedEdgeCount = 0;
	PrimaryActorTick.bCanEverTick = true;
	

}

// Called when the game starts or when spawned
void AP_MazeCell::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AP_MazeCell::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );

}

void AP_MazeCell::SetEdge(Direction dir, AP_MazeCellEdge* edge)
{
	this->Edges[(int32)dir] = edge;
	this->InitializedEdgeCount += 1;
}

AP_MazeCellEdge* AP_MazeCell::GetEdge(Direction dir)
{
	return Edges[(int32)dir];
}

bool AP_MazeCell::IsFullyInitialized()
{
	return (InitializedEdgeCount == 4);
}

Direction AP_MazeCell::RandomUninitializedDirection()
{
	int32 skips = FMath::RandRange(0, 3 - InitializedEdgeCount);

	for (int i = 0; i < 4; i++)
		{
			if (Edges[i] == NULL)
			{
				if (skips == 0)
				{
					return (Direction)i;
				}
				skips -= 1;
			}
		}
		return (Direction)0;

}
