
#pragma once
#include "GameFramework/Actor.h"
#include "P_MazeDirection.generated.h"

UENUM()
enum Direction
{
	North,
	East,
	South,
	West

};

class P_MazeDirection
{
public:
	
	const int32 Count = 4;
	TArray<FIntVector> directionVectors;
	TArray<Direction> opposites;
	TArray<FRotator> rotations;
	
	P_MazeDirection()
	{
		directionVectors.Add(FIntVector(0, 1, 0));
		directionVectors.Add(FIntVector(1, 0, 0));
		directionVectors.Add(FIntVector(0, -1, 0));
		directionVectors.Add(FIntVector(-1, 0, 0));

		opposites.Add(Direction::South);
		opposites.Add(Direction::West);
		opposites.Add(Direction::North);
		opposites.Add(Direction::East);

		rotations.Add(FRotator::ZeroRotator);
		rotations.Add(FRotator(0,90,0));
		rotations.Add(FRotator(0, 180, 0));
		rotations.Add(FRotator(0,270,0));

		//MazeDirection = Direction::North;
		//OppositeDirection = Direction::South;
	}
	Direction RandomDirection()
	{
		return (Direction)FMath::RandRange(0,Count-1);
	}
	FIntVector ToFIntVector(Direction dir)
	{
		return directionVectors[(int32)dir];
	}
	Direction GetOpposite(Direction dir)
	{
		return opposites[(int32)dir];
	}

	FRotator ToRotation(Direction dir)
	{
		return rotations[(int32)dir];
	}
};