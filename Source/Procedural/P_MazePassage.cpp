// Fill out your copyright notice in the Description page of Project Settings.

#include "Procedural.h"
#include "P_MazePassage.h"


// Sets default values
AP_MazePassage::AP_MazePassage(const FObjectInitializer& ObjectInitializer) :Super(ObjectInitializer)
{
	_m_scene = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("SceneComp"));
	RootComponent = _m_scene;

	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AP_MazePassage::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AP_MazePassage::Tick(float DeltaTime)
{
	Super::Tick( DeltaTime );

}

