// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "P_MazeCellEdge.h"
#include "P_MazePassage.generated.h"

UCLASS()
class PROCEDURAL_API AP_MazePassage : public AP_MazeCellEdge
{
	GENERATED_BODY()
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class USceneComponent* _m_scene;
		
public:	
	

	// Sets default values for this actor's properties
	AP_MazePassage(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;
		
};
