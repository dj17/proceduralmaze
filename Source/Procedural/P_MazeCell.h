// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "P_MazeDirection.h"
#include "P_MazeCell.generated.h"

class AP_MazeCellEdge;

UCLASS()
class PROCEDURAL_API AP_MazeCell : public AActor
{
	GENERATED_BODY()
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* _scene;
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* _floor;
		
public:	

	// Sets default values for this actor's properties
	AP_MazeCell(const FObjectInitializer& ObjectInitializer);

	FIntVector Coordinates;

	TArray<AP_MazeCellEdge*> Edges;

	int32 InitializedEdgeCount;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void SetEdge(Direction dir, AP_MazeCellEdge* edge);
	
	AP_MazeCellEdge* GetEdge(Direction dir);
	
	bool IsFullyInitialized();

	Direction RandomUninitializedDirection();
};
